
package com.auth.controller;
  
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
  
@Controller
public class LoginController {
      
    @GetMapping("/welcome")
    public String welcome() {
        return "Welcome";
          
    }
      
    @GetMapping("/login")
    public String login() {
        return "loginPage";
          
    }
}
